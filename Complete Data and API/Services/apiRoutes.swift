//
//  apiRoutes.swift
//  Complete Data and API
//
//  Created by Pedja Jevtic on 11/22/17.
//  Copyright © 2017 Pedja Jevtic. All rights reserved.
//

import Foundation

enum API_Paths: String {
    
    case music_albums
    // add more paths/cases for complete api communication
    
}
